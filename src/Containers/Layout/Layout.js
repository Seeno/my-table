import React, { useState, useEffect, useRef } from 'react';

import tableData from '../../assets/data/data';
import DataTable from '../../components/DataTable/DataTable';
import UserInput from '../../components/UserInput/UserInput';
import AddToTable from '../../components/hooks/AddToTable/AddToTable';
import RemoveFromTable from '../../components/hooks/RemoveFromTable/RemoveFromTable';
import CalculateAge from '../../components/hooks/CalculateAge/CalculateAge';
import ValidateForm from '../../components/hooks/ValidateForm/ValidateForm'  

const Layout = () => {
  const [data, setData] = useState(
      tableData.map(data => {
          let obj = { col1: null, col2: null, col3: null, col4: null};
          obj.id = data.id
          obj.col1 = data.name;
          obj.col2 = data.age;
          obj.col3 = data.mobile;
          obj.col4 = data.birthDate;
          obj.col5 = "DELETE";
          return obj;
      })
  );
  const [name, setName] = useState(null);
  const [phone, setPhone] = useState(null);
  const [birthDate, setBirthDate] = useState(null);
  const [errors, setErrors] = useState({
    name: true,
    phone: true,
    birthDate: true
  });

  const columns =[
      {
      Header: 'name',
      accessor: 'col1', // accessor is the "key" in the data
      },
      {
      Header: 'age',
      accessor: 'col2',
      },
      {
      Header: 'phone number',
      accessor: 'col3',
      },
      {
      Header: 'birth date',
      accessor: 'col4',
      },
      {
      Header: 'DELETE',
      accessor: 'col5',
      }
  ];

  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      mounted.current = true;
    } else {
      const newErrors = ValidateForm(name, phone, birthDate);
      setErrors(newErrors);
    }
  }, [name, phone, birthDate])
  
  const submitHandle = (event) => {
    event.preventDefault();
    if(!errors.name && !errors.phone && !errors.birthDate) {
      console.log(true);
      const user = {
        id: null,
        name: name,
        age: CalculateAge(birthDate),
        mobile: phone,
        birthDate:birthDate
      };
      const newData = AddToTable(data, user);
      setData(newData);
      alert("You successfully added " + user.name + " to the table. Congrats!");
    } else {
      console.log(false);
      alert("the form is invalid");
    }

  }

  const rowDeleteHandler = (id) => {
    const name = data[id].col1;
    if (window.confirm("Are you sure you want to delete " + name + " from your table?") == true) {
      const newData = RemoveFromTable(data, id);
      setData(newData);
      alert("You successfully removed " + name + " from the table. Congrats!");
    }
  }

  const nameChangeHandler = (event) => {
    setName(event.target.value);
  }

  const phoneChangeHandler = (event) => {
    setPhone(event.target.value)
  }

  const birthDateChangeHandler = (event) => {
    setBirthDate(event.target.value)
  }

  return (
      <React.Fragment>
          <DataTable 
              rowDelete={(id) => rowDeleteHandler(id)}
              tableData={data} 
              tableColumns={columns}
          />
          <UserInput 
              inputErrors={errors}
              submit={submitHandle}
              birthDateChange={birthDateChangeHandler}
              phoneChange={phoneChangeHandler}
              nameChange={nameChangeHandler}
          />
      </React.Fragment>
  );
};

export default Layout;