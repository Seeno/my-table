import React from 'react';

import { useTable, useGlobalFilter, useSortBy, usePagination } from 'react-table';
import GlobalFilter from './GlobalFilter/GlobalFilter';
import {Table, Row, Col, Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

const DataTable = (props) => {
    const data = props.tableData;
    
    const columns = props.tableColumns;

    const tableInstance = useTable({ columns, data }, useGlobalFilter, useSortBy, usePagination);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        nextPage,
        previousPage,
        prepareRow,
        canPreviousPage,
        canNextPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        state,
        setGlobalFilter,
    } = tableInstance;

    const { globalFilter } = state;

    const { pageIndex, pageSize } = state;

    return (
        <>
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <GlobalFilter 
                        filter={globalFilter}
                        setFilter={setGlobalFilter}
                    />
                </Col>
            </Row>
            <Table 
                className='mt-3'
                striped bordered hover
                {...getTableProps()}
            >
                <thead>
                    {// Loop over the header rows
                    headerGroups.map(headerGroup => (
                        // Apply the header row props
                        <tr {...headerGroup.getHeaderGroupProps()}>
                        {// Loop over the headers in each row
                        headerGroup.headers.map(column => (
                            // Apply the header cell props
                            <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                              {column.render('Header')}
                              <span>
                                  {column.isSorted ? (column.isSortedDesc ? ' ▼' : ' ▲') : ''}
                              </span>
                            </th>
                        ))}
                        </tr>
                    ))}
                </thead>
                {/* Apply the table body props */}
                <tbody {...getTableBodyProps()}>
                    {// Loop over the table rows
                    page.map(row => {
                        // Prepare the row for display
                        prepareRow(row)
                        return (
                        // Apply the row props
                        <tr
                            {...row.getRowProps()}
                        >
                            {// Loop over the rows cells
                            row.cells.map(cell => {
                            // Apply the cell props
                            return (
                                <td 
                                    //onClick={props.rowDelete(row.id)}
                                    onClick={() => {
                                        cell.value === "DELETE" ? props.rowDelete(row.id) : console.log(cell);
                                    }}
                                    {...cell.getCellProps()}
                                >
                                {// Render the cell contents
                                cell.render('Cell')}
                                </td>
                            )
                            })}
                        </tr>
                        )
                    })}
                </tbody>
            </Table>
            <Row>
                <Col xs={12}>
                    <div>
                        <span>
                            page{' '}
                            <strong>
                                {pageIndex + 1} of {pageOptions.length}
                            </strong>{' '}
                        </span>
                        <span>
                            | Go to page: {' '}
                            <input 
                                style={{ width: '50px' }}
                                type='number' 
                                defaultValue={pageIndex + 1} 
                                onChange={e => {
                                    const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                                    gotoPage(pageNumber)
                                }}
                            />
                        </span>
                        <select 
                            value={pageSize}
                            onChange={e => setPageSize(Number(e.target.value))}
                        >
                            {
                                [4, 6, 10, 20].map(pageSize => (
                                    <option 
                                        key={pageSize}
                                        value={pageSize}
                                    >
                                        Show {pageSize}
                                    </option>
                                ))
                            }
                        </select>
                        <Button
                            variant="secondary"
                            onClick={() => gotoPage(0)}
                            disabled={!canPreviousPage}
                        >{'<<'}</Button>
                        <Button 
                            variant="secondary"
                            onClick={() => previousPage()}
                            disabled={!canPreviousPage}
                        >previous</Button>
                        <Button 
                            variant="secondary"
                            onClick={() => nextPage()}
                            disabled={!canNextPage}
                        >next</Button>
                        <Button
                            variant="secondary"
                            onClick={() => gotoPage(pageCount - 1)}
                            disabled={!canNextPage}
                        >{'>>'}</Button>
                    </div>
                </Col>
            </Row>
           
        </>
    );
};

export default DataTable;