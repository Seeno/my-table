import React from 'react'

import {Form} from 'react-bootstrap';

const globalFilter = (props) => {
    return (
        <span>
            Search: {' '}
            <Form.Control
                value={props.filter || ''} 
                onChange={e => props.setFilter(e.target.value)}
            ></Form.Control>
        </span>
    )
}

export default globalFilter;