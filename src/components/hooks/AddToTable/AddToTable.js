const addToTable = (dataList, newData) => {
    const list = [...dataList]
    const id = list[list.length - 2].id + 2;
    let newListItem = {
        id: id,
        col1: newData.name,
        col2: newData.age,
        col3: newData.mobile,
        col4: newData.birthDate,
        col5: 'DELETE'
    };
    list.push(newListItem);
    return list;
};

export default addToTable;