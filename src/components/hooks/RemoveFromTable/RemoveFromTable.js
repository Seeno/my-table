const removeFromTable = (dataList, index) => {
    const list = [...dataList];
    list.splice(index, 1);
    return list;
}

export default removeFromTable;