import CalculateAge from '../CalculateAge/CalculateAge'

const validateForm = (name, phone, birthDate) => {
    let errors = {
        name: false,
        phone: false,
        birthDate: false,
    };
    if (typeof name !== 'string' || name.length === 0) {
        errors.name = true;
    }
    if (typeof phone !== 'string' || phone.length < 11 || phone.length > 11) {
        errors.phone = true;
    }
    errors.birthDate = validateDate(birthDate);
    return errors;
};

const validateDate = (date) => {
    if (date) {
        const splitDate = date.split('/');
        const age = CalculateAge(date);
        if (
            splitDate.length !== 3 ||
            splitDate[1] < 1 ||
            splitDate[1] > 12 ||
            splitDate[2] < 1 ||
            splitDate[2] > 31 ||
            age < 0
        ) {
            return true;
        } else {
            return false;
        }
    }
}

export default validateForm;