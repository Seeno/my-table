import React from 'react';

import {Container, Row, Col, Form, Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import PropTypes from 'prop-types';

const userInput = (props) => {
    const errors = props.inputErrors;

    return (
        <Container className='mt-3'>
            <Row>
                <Col md={{ span: 6, offset: 3 }}>
                    <Form>
                        <Form.Group controlId="formName"> 
                            <Form.Label
                                className='mt-3'
                                style={{color: 'red'}}
                                hidden={!errors.name}
                            >please enter a name.</Form.Label>
                            <Form.Control
                                isInvalid={errors.name}
                                className='mt-1'
                                placeholder="your name"
                                onChange={props.nameChange}
                            ></Form.Control>
                        </Form.Group>

                        <Form.Group controlId="formPhone">
                            <Form.Label
                                className='mt-3'
                                style={{color: 'red'}}
                                hidden={!errors.phone}
                            >phone number should be 11 digits!</Form.Label>
                            <Form.Control
                                isInvalid={errors.phone}
                                className='mt-1'
                                placeholder="your phone number"
                                onChange={props.phoneChange}
                            ></Form.Control>
                        </Form.Group>

                        <Form.Group controlId="formBirthDate">
                            <Form.Label
                                className='mt-3'
                                style={{color: 'red'}}
                                hidden={!errors.birthDate}
                            >birth date is not valid! please enter a valid date.</Form.Label>
                            <Form.Control 
                                isInvalid={errors.birthDate}
                                className='mt-1'
                                placeholder="your birth date"
                                onChange={props.birthDateChange}
                            ></Form.Control>
                        </Form.Group>
                        
                        <Button 
                            className='mt-3'
                            style={{display: 'block', margin: 'auto'}}
                            onClick={props.submit}
                        >Submit</Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
};

userInput.propTypes = {
    nameChange: PropTypes.func,
    phoneChange: PropTypes.func,
    birthDateChange: PropTypes.func,
};

export default userInput;